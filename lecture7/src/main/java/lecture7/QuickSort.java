package lecture7;

import java.util.concurrent.ThreadLocalRandom;
import java.util.*;

public class QuickSort {
    public static void quickSort(int[] A, int l, int r) {
        // TODO
        throw new UnsupportedOperationException("Not implemented");
    }

    public static int partition(int[] A, int l, int r) {
        // TODO
        // Hint: Implement the swap method below first and use it.
        // That will make this task simpler.
        throw new UnsupportedOperationException("Not implemented");
    }

    static void swap(int[] A, int i, int j) {
        // TODO
        throw new UnsupportedOperationException("Not implemented");
    }

    public static int hoarePartition(int[] A, int l, int r) {
        // TODO
        throw new UnsupportedOperationException("Not implemented");
    }

    public static int[] randomArray(int n) {
        int[] array = new int[n];

        for (int i = 0; i < n; i++) {
            array[i] = ThreadLocalRandom.current().nextInt(0, 30);
        }
        return array;
    }

    public static void main(String[] args) {
        int[] array = randomArray(17);
        System.out.println(Arrays.toString((array)));

        quickSort(array, 0, array.length - 1);

        System.out.println(Arrays.toString((array)));
    }

}

package lecture7;

public class DirectAddressing {

    public static class Data {

        public final int key;
        public final String data;

        public Data(int key, String data) {
            this.data = data;
            this.key = key;
        }
    }

    private Data[] buffer;

    DirectAddressing(int m) {
        this.buffer = new Data[m];
    }

    void insert(Data x) {
        // TODO
    }

    void remove(Data x) {
        // TODO
    }

    Data search(int key) {
        // TODO
        throw new UnsupportedOperationException("not implemented");
    }

}

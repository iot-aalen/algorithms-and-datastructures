package lecture8;

public class Element {
    public final String data;
    public final int key;

    Element prev;
    Element next;

    Element(String data, int key) {
        this.data = data;
        this.key = key;
        this.prev = null;
        this.next = null;
    }

}

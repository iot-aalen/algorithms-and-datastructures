/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package lecture8;

import java.util.concurrent.ThreadLocalRandom;
import java.util.*;

public class App {
    public static int[] randomArray(int n) {
        int[] array = new int[n];

        for (int i = 0; i < n; i++) {
            array[i] = ThreadLocalRandom.current().nextInt(0, 30);
        }
        return array;
    }

    public static void main(String[] args) {
        int[] array = randomArray(17);
        System.out.println(Arrays.toString((array)));

        System.out.println(Arrays.toString((array)));
    }
}

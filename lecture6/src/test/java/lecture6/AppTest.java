/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package lecture6;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Disabled;
import static org.junit.jupiter.api.Assertions.*;

public class AppTest {
    @Test
    @Disabled
    public void testMerge() {
        int[] array = { 0, 5, 13, 13, 18, 5, 15, 16, 16, 18 };
        MergeSort.merge(array, 0, 4, 9);
        assertArrayEquals(array, new int[] { 0, 5, 5, 13, 13, 15, 16, 16, 18, 18 });
    }

    @Test
    @Disabled
    public void testMergeSort() {
        int[] array = { 1, 2, 19, 14, 5, 7, 14, 8, 19, 9 };
        MergeSort.mergeSort(array, 0, array.length - 1);
        assertArrayEquals(array, new int[] { 1, 2, 5, 7, 8, 9, 14, 14, 19, 19 });
    }

    @Test
    @Disabled
    public void testBinarySearch() {
        int[] array = new int[] { 1, 2, 5, 7, 8, 9, 14, 14, 19, 19 };
        assertEquals(BinarySearch.binarySearch(array, 3, 0, array.length - 1), -1);
        assertEquals(BinarySearch.binarySearch(array, 2, 0, array.length - 1), 1);
        assertEquals(BinarySearch.binarySearch(array, 14, 0, array.length - 1), 7);
    }

    @Test
    @Disabled
    public void recursionTest() {
        int[] array = new int[] {1,2,3,4,5};
        assertEquals(RecursionExercise.sum(array, 0),15);
    }
}

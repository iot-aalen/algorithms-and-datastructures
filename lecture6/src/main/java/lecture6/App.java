/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package lecture6;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class App {
    public static int[] randomArray(int n) {
        int[] array = new int[n];

        for (int i = 0; i < n; i++) {
            array[i] = ThreadLocalRandom.current().nextInt(0, 30);
        }
        return array;
    }

    public static void main(String[] args) {
        int[] array = randomArray(17);

        System.out.println(Arrays.toString((array)));

        /*
         * WICHTIG: Beachten Sie, dass die Werte 0 und array.length - 1 unten beides
         * gültige Array Indizes sind.
         *
         * Wenn Sie die Konvention beihbehalten, dass merge und mergeSort nur gültige
         * Array indizes als Parameter erhält, ist die Implementierung relativ einfach
         * (mit den Folien).
         */
        MergeSort.mergeSort(array, 0, array.length - 1);

        System.out.println(Arrays.toString((array)));
    }
}

public class List {
    public static class ListElement {

        public ListElement next = null;
        public ListElement prev = null;
        public String key;

    }

    private ListElement head = null;

    public ListElement getHead() {
        return head;
    }

    public ListElement insert(String key) {
        //TODO
        throw new UnsupportedOperationException("not implemented");
    }

    public void remove(ListElement x) {
        //TODO
        throw new UnsupportedOperationException("not implemented");
    }

    public ListElement search(String key) {
        //TODO
        throw new UnsupportedOperationException("not implemented");
    }

    public void printList() {
        ListElement x = head;
        while (x != null) {
            System.out.print(x.key + " -> ");
            x = x.next;
        }
        System.out.println("");
    }

}

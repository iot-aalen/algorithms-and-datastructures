public class Queue {

    public static class QueueUnderrunException extends java.lang.Exception {
    }

    public static class QueueOverrunException extends java.lang.Exception {
    }

    private final int[] buffer;

    private int head = 0;
    private int tail = 0;

    public Queue(int capacity) {
        buffer = new int[capacity];
    }
    /*
     * HINWEIS: Nutzen Sie 'throw new QueueUnderrunException();' bzw 'throw new
     * QueueOverrunException();' um die Fehlerfälle zu behandeln. ACHTUNG: Die
     * Fehlerfälle sind im Pseudocode nicht erwähnt. Sie müssen Sie selbst finden
     * und implementieren.
     */

    public void enqueue(int x) throws QueueOverrunException {
        // TODO
        throw new UnsupportedOperationException("not implemented");
    }

    public int dequeue() throws QueueUnderrunException {
        // TODO
        throw new UnsupportedOperationException("not implemented");
    }

}

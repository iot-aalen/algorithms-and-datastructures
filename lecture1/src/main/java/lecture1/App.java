package lecture1;

import java.util.Arrays;

public class App {

    private static int[] parseArray(String[] args, int offset) throws NumberFormatException {
        if (offset > args.length) {
            throw new IllegalArgumentException("Offset too large");
        }
        int[] result = new int[args.length - offset];
        for (int i = offset; i < args.length; i++) {
            result[i - offset] = Integer.parseInt(args[i]);
        }
        return result;
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Error. need one argument of 'extendedEuclid', 'reverse_inplace', or 'reverse_alloc'");
            System.exit(1);
        }
        final String command = args[0];
        switch (command) {
        case "extendedEuclid":
            try {
                if (args.length < 3) {
                    System.err.println("Need to numbers in decimal format. You did not specify enough parameters");
                    System.exit(1);
                }
                final int a = Integer.parseInt(args[1]);
                final int b = Integer.parseInt(args[2]);
                Tasks.Triple result = Tasks.extendedEuclideanAlgorithm(a, b);
                System.out.printf("Result: %d, %d, %d\n", result.first, result.second, result.third);
            } catch (NumberFormatException e) {
                System.err.println("Need two numbers in decimal format.");
                System.exit(1);
            }
            break;
        case "reverse_inplace":
            try {
                int[] array = parseArray(args, 1);
                Tasks.reverse_inplace(array);
                System.out.println("Result: " + Arrays.toString(array));
            } catch (NumberFormatException e) {
                System.err.println("Not a number: " + e.getMessage());
                System.exit(1);
            }
            break;
        case "reverse_alloc":
            try {
                int[] array = parseArray(args, 1);
                int[] result = Tasks.reverse_alloc(array);
                System.out.println("Result: " + Arrays.toString(result));
            } catch (NumberFormatException e) {
                System.err.println("Not a number: " + e.getMessage());
                System.exit(1);
            }
            break;
        default:
            System.err.println("Error. need one argument of 'extendedEuclid', 'reverse_inplace', or 'reverse_alloc'");
            System.exit(1);
        }
    }
}

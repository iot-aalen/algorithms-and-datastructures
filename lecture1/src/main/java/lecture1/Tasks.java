package lecture1;

public class Tasks {

    public static class Triple {
        public final int first;
        public final int second;
        public final int third;

        public Triple(int first, int second, int third) {
            this.first = first;
            this.second = second;
            this.third = third;
        }
    }

    public static Triple extendedEuclideanAlgorithm(int a, int b) {
        throw new UnsupportedOperationException("not implemented");
        // hier Ihre Implementierung
    }

    public static void reverse_inplace(int[] array) {
        throw new UnsupportedOperationException("not implemented");
        // hier Ihre Implementierung
    }

    public static int[] reverse_alloc(int[] array) {
        throw new UnsupportedOperationException("not implemented");
        // hier Ihre Implementierung
    }

}

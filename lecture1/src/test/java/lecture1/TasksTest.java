package lecture1;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Disabled;

public class TasksTest {
    @Test
    @Disabled
    public void extendedEuclidean() {
        Tasks.Triple result = Tasks.extendedEuclideanAlgorithm(2, 3);
        assertEquals(result.first, 1);
        assertEquals(result.second, -1);
        assertEquals(result.third, 1);

        result = Tasks.extendedEuclideanAlgorithm(20, 35);
        assertEquals(result.first, 5);
        assertEquals(result.second, 2);
        assertEquals(result.third, -1);
    }

    @Test
    @Disabled
    public void test_reverse_alloc() {
        int[] a = { 1, 2, 3, 4, 5, 6 };
        int[] result = Tasks.reverse_alloc(a);
        assertNotSame(a, result);
        assertTrue(Arrays.equals(result, new int[] { 6, 5, 4, 3, 2, 1 }));
        a = new int[] { 1, 2, 3, 4, 5 };
        result = Tasks.reverse_alloc(a);
        assertNotSame(a, result);
        assertTrue(Arrays.equals(result, new int[] { 5, 4, 3, 2, 1 }));
    }

    @Test
    @Disabled
    public void test_reverse_inplace() {
        int[] a = { 1, 2, 3, 4, 5, 6 };
        Tasks.reverse_inplace(a);
        assertTrue(Arrays.equals(a, new int[] { 6, 5, 4, 3, 2, 1 }));
        a = new int[] { 1, 2, 3, 4, 5 };
        Tasks.reverse_inplace(a);
        assertTrue(Arrays.equals(a, new int[] { 5, 4, 3, 2, 1 }));
    }
}

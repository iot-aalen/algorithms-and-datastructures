package lecture9;

public class BinaryTree {

    public TreeNode root = null;

    public int height() {
        return TreeUtils.recursiveHeight(root) - 1;
    }

    public void inorderTreeWalk() {
        inorderTreeWalk(root);
    }

    public void preorderTreeWalk() {
        preorderTreeWalk(root);
    }

    public void postorderTreeWalk() {
        postorderTreeWalk(root);
    }

    public TreeNode minimum() {
        return minimum(root);
    }

    public TreeNode maximum() {
        return maximum(root);
    }

    private void inorderTreeWalk(TreeNode node) {
        // TODO
        throw new UnsupportedOperationException("Not implemented");
    }

    private void preorderTreeWalk(TreeNode node) {
        // TODO
        throw new UnsupportedOperationException("Not implemented");
    }

    private void postorderTreeWalk(TreeNode node) {
        // TODO
        throw new UnsupportedOperationException("Not implemented");
    }

    public TreeNode search(int key) {
        // TODO
        throw new UnsupportedOperationException("Not implemented");
    }

    public TreeNode search_recursive(int key, TreeNode node) {
        // TODO
        throw new UnsupportedOperationException("Not implemented");
    }

    private TreeNode minimum(TreeNode current) {
        // TODO
        throw new UnsupportedOperationException("Not implemented");
    }

    private TreeNode maximum(TreeNode current) {
        // TODO
        throw new UnsupportedOperationException("Not implemented");
    }

    public TreeNode predecessor(TreeNode x) {
        // TODO
        throw new UnsupportedOperationException("Not implemented");
    }

    public TreeNode successor(TreeNode x) {
        // TODO
        throw new UnsupportedOperationException("Not implemented");
    }

    public void insert(TreeNode x) {
        // TODO
    }

    @Override
    public String toString() {
        return toString(root);
    }

    private static String toString(TreeNode node) {
        if (node == null) {
            return "";
        }
        return "(" + toString(node.getLeftChild()) + node.getData() + toString(node.getRightChild()) + ")";
    }

}

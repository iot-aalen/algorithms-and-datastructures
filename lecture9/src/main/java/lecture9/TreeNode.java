package lecture9;

public class TreeNode {

    private TreeNode parent = null;
    private TreeNode leftChild = null;
    private TreeNode rightChild = null;
    public int key;
    public Object data;

    public TreeNode(int key, Object o) {
        this.key = key;
        this.data = o;
    }

    public TreeNode() {

    }

    public int getKey() {
        return this.key;
    }

    public Object getData() {
        return this.data;
    }

    public TreeNode getParent() {
        return parent;
    }

    public TreeNode getLeftChild() {
        return leftChild;
    }

    public TreeNode getRightChild() {
        return rightChild;
    }

    public void setParent(TreeNode parent) {
        this.parent = parent;
    }

    public void setLeftChild(TreeNode leftChild) {
        this.leftChild = leftChild;
    }

    public void setRightChild(TreeNode rightChild) {
        this.rightChild = rightChild;
    }

}

package lecture9;

import java.util.Stack;

public class TreeUtils {

    private static class TreeParser {
        private Stack<TreeNode> stack = new Stack<>();
        private final String tree;
        private TreeNode current;

        TreeParser(String tree) {
            this.tree = tree;
        }

        BinaryTree parseTree() {
            if (tree == null) {
                return null;
            }
            BinaryTree t = new BinaryTree();
            for (int pos = 0; pos < tree.length();) {
                switch (tree.charAt(pos)) {
                    case '(':
                        openBracket();
                        pos++;
                        break;
                    case ')':
                        closeBracket();
                        pos++;
                        break;
                    case ' ':
                        pos++;
                        continue; // skip whitespace
                    default:
                        pos = consumeInteger(pos);
                }
            }
            if (!stack.isEmpty()) {
                throw new IllegalStateException("stack not empty");
            }
            t.root = current;
            return t;
        }

        private int consumeInteger(int pos) {
            int j = pos;
            consume: for (; j < tree.length(); j++) {
                switch (tree.charAt(j)) {
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        continue;
                    default:
                        break consume;
                }
            }
            if (j == tree.length()) {
                throw new IllegalStateException("Cannot parse this");
            }
            String keyString = tree.substring(pos, j);
            int key = Integer.parseInt(keyString);
            TreeNode top = stack.peek();
            top.key = key;
            top.data = keyString;
            return j;
        }

        private void openBracket() {
            TreeNode n = new TreeNode();
            if (!stack.isEmpty()) {
                TreeNode top = stack.peek();
                if (top.data == null) {
                    top.setLeftChild(n);
                } else {
                    top.setRightChild(n);
                }
                n.setParent(top);
            }
            stack.push(n);
        }

        private void closeBracket() {
            current = stack.pop();
        }
    }

    public static BinaryTree parseTree(String tree) {
        TreeParser p = new TreeParser(tree);
        return p.parseTree();
    }

    static int recursiveHeight(TreeNode node) {
        if (node == null) {
            return 0;
        }
        return 1 + Math.max(recursiveHeight(node.getLeftChild()), recursiveHeight(node.getRightChild()));
    }
}

/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package lecture3;

import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.Disabled;
import static org.junit.jupiter.api.Assertions.*;

public class AppTest {
    @Test
    @Disabled
    public void testStackOverflow() throws Stack.StackOverflowException {
        Stack stack = new Stack(0);
        assertThrows(Stack.StackOverflowException.class, () -> stack.push("There is no room on this stack"));
    }

    @Test
    @Disabled
    public void testStackUnderflow() throws Stack.StackUnderflowException {
        Stack s = new Stack(2);
        assertThrows(Stack.StackUnderflowException.class, () -> s.pop()); // this should trigger a stack underflow
    }

    @Test
    @Disabled
    public void normalStackOperation() {
        final String[] elements = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
        Stack s = new Stack(20);
        for (int i = 0; i < elements.length; i++) {
            try {
                s.push(elements[i]);
            } catch (Stack.StackOverflowException e) {
                System.err.printf("Error: This is iteration %d. Message was %s\n", i, e.toString());
                fail();
            }
        }
        for (int i = elements.length - 1; i >= 0; i--) {
            try {
                assertEquals(s.pop(), elements[i]);
            } catch (Stack.StackUnderflowException e) {
                System.err.printf("Underflow in iteration %d\n", elements.length - i);
                fail("Stack underflow was unexpected");
            }
        }

    }
}

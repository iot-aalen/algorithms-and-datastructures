package lecture3;

public class Stack {

    private final String[] buffer;

    // TODO
    // private int position = ???;

    public static class StackOverflowException extends Exception {
    }

    public static class StackUnderflowException extends Exception {
    }

    /*
     * HINWEIS: Nutzen Sie 'throw new StackOverflowException();' bzw 'throw new
     * StackUnderflowException();' um die Fehlerfälle zu behandeln (dort, wo im
     * Pseudocode das Schlüsselwort 'error' genutzt wird.
     */

    /**
     * @param[capacity] Die Größe des Stacks, also wie viele Elemente er aufnehmen
     *                  kann, bevor es zum StackOverflow kommt.
     */
    public Stack(int capacity) {
        buffer = new String[capacity];
    }

    public void push(String x) throws Stack.StackOverflowException {
        // TODO
        throw new UnsupportedOperationException("not implemented");
    }

    public String pop() throws Stack.StackUnderflowException {
        // TODO
        throw new UnsupportedOperationException("not implemented");
    }
}
